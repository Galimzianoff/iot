/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * Copyright 2016-2017 NXP
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* FreeRTOS kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

/* Freescale includes. */
#include "fsl_device_registers.h"
#include "fsl_debug_console.h"
#include "board.h"

#include "pin_mux.h"
#include <stdbool.h>
#include "clock_config.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define BOARD_LED_GPIO GPIOB
#define BOARD_LED_GPIO_PIN 1U

/* Task priorities. */
#define hello_task_PRIORITY (configMAX_PRIORITIES - 1)
/*******************************************************************************
 * Prototypes
 ******************************************************************************/
static void hello_task(void* pvParameters);
static void hello_task2(void* pvParameters);
/*******************************************************************************
 * Code
 ******************************************************************************/
/*!
 * @brief Application entry point.
 */
int main(void)
{
    /* Init board hardware. */
	gpio_pin_config_t led_config = {
			kGPIO_DigitalOutput, 0,
	};
	GPIO_PinInit(BOARD_LED_GPIO, BOARD_LED_GPIO_PIN, &led_config);

    BOARD_InitPins();
    BOARD_BootClockRUN();
    BOARD_InitDebugConsole();
	
	xTaskCreate(hello_task, "Hello_task", 3 * configMINIMAL_STACK_SIZE + 30, NULL, hello_task_PRIORITY, NULL);
	xTaskCreate(hello_task2, "Hello_task2", 3 * configMINIMAL_STACK_SIZE + 30, NULL, hello_task_PRIORITY, NULL);
    vTaskStartScheduler();
    for (;;)
        ;
}
/*!
 * @brief Task responsible for printing of "Hello world." message.
 */
int count(i, time)
{
	static int count1 = 0;
	static int countAppend = 0;
	if (i == 0) {
		return count1;
	}
	else if (countAppend % 2 == 0 && i != 0) {
		count1 += time;
		return count1;
	}
}

static void hello_task(void* pvParameters)
{
	for (;;)
	{
		int timeSwitch = 3000 + rand() % 8000;
		PRINTF(("Change voltage\r\n"));
		GPIO_TogglePinsOutput(BOARD_LED_GPIO, 1u << BOARD_LED_GPIO_PIN);
		count(1, timeSwitch);
		vTaskDelay(pdMS_TO_TICKS(timeSwitch));
	}
}

void hello_task2(void* pvParameters)
{
	for (;;)
	{
		int answer = 0;
		answer = count(0);
		char string[20] = "";
		_itoa(answer, string, 10);
		PRINTF((string));
		vTaskDelay(pdMS_TO_TICKS(5000));
	}
}

#include "fsl_port.h"
void NMI_Handler(void)
{
    /* Change NMI Pin MUX */
    CLOCK_EnableClock(kCLOCK_PortB);
    PORT_SetPinMux(PORTB, 18, kPORT_MuxAlt2);
}


